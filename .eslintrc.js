module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module',
  },
  extends: 'airbnb-base',
  rules: {
    semi: [2, "always"],
    "comma-dangle": [2, "always-multiline"],
    "no-console": 0,
  },
  plugins: [],
  env: {
    jest: true,
  },
}
