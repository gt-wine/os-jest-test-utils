# Winecraft Express Response Generator


## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Versioning](#versioning)
- [Support](#support)
- [Contributing](#contributing)

## Installation

### General Setup
```
$ npm install
```

## Usage

mocks, utils for testing with Jest.
```
# example tbd
```

## Versioning

This project uses semantic versioning - https://semver.org/

To create a new version run the following command

```
$ npm version {major|minor|patch}
```

## Support

Adam Synnott <adam.synnott@winecraft.com>

## Contributing

Please contribute using [Github Flow](https://guides.github.com/introduction/flow/).
